package userede.com.br.CadastroFilmes.dtos;

import java.time.LocalDate;
import java.util.ArrayList;

public class FilmeDTO {

    private String nome;
    private String sinopse;
    private LocalDate anoDeLancamento;
    private Enum categoria;
   // private List<Ator> Atores  = new ArrayList<>();

    public FilmeDTO (){

    }

    public FilmeDTO(String nome, String sinopse, LocalDate anoDeLancamento, Enum categoria) {
        this.nome = nome;
        this.sinopse = sinopse;
        this.anoDeLancamento = anoDeLancamento;
        this.categoria = categoria;
    }

    public String getNome() {
        return nome;
    }

    public String getSinopse() {
        return sinopse;
    }

    public LocalDate getAnoDeLancamento() {
        return anoDeLancamento;
    }

    public Enum getCategoria() {
        return categoria;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public void setAnoDeLancamento(LocalDate anoDeLancamento) {
        this.anoDeLancamento = anoDeLancamento;
    }

    public void setCategoria(Enum categoria) {
        this.categoria = categoria;
    }
}
